package ru.sav.h2program;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;
import ru.sav.h2program.dao.AuthorDaoJdbc;
import ru.sav.h2program.model.Author;

import static org.assertj.core.api.Assertions.assertThat;


@DisplayName("Тест методов AuthorDaoJdbc")
@JdbcTest
@Import(AuthorDaoJdbc.class)
public class AuthorDaoJdbcTests {

    @Autowired
    private AuthorDaoJdbc jdbc;

    @DisplayName("Получение книги по id")
    @Test
    void shouldGetAuthorFromDBById() {
        Author author = jdbc.getById(1);
        assertThat(author).hasFieldOrPropertyWithValue("fio", "Petrov Valeriy Ivanovich");
    }

    @DisplayName("Получение книги по fio")
    @Test
    void shouldGetAuthorFromDBByFio() {
        Author author = jdbc.getByFio("Petrov Valeriy Ivanovich");
        assertThat(author).hasFieldOrPropertyWithValue("id", 1L);
    }

    @DisplayName("возвращает новую созданную книгу")
    @Test
    void shouldReturnNewAuthor () {
        Author author = new Author("Sokolov Alexey Vladimirovich", null);
        jdbc.create(author);
        Author authorFromDB = jdbc.getByFio("Sokolov Alexey Vladimirovich");
        assertThat(authorFromDB).hasFieldOrPropertyWithValue("fio", "Sokolov Alexey Vladimirovich");
    }

}


