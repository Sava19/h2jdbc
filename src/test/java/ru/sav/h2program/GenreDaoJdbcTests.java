package ru.sav.h2program;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;
import ru.sav.h2program.dao.GenreDaoJdbc;
import ru.sav.h2program.model.Genre;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Тест методов GenreDaoJdbc")
@JdbcTest
@Import(GenreDaoJdbc.class)
public class GenreDaoJdbcTests {

    @Autowired
    private GenreDaoJdbc jdbc;

    @DisplayName("получает жанр по id")
    @Test
    void shouldGetGenreFromDBById() {
        Genre genre = jdbc.getById(1);
        assertThat(genre).hasFieldOrPropertyWithValue("genre", "Story");
    }

    @DisplayName("возвращает новый созданный жанр")
    @Test
    void shouldReturnNewAGenre () {
        Genre genre = new Genre("Novel");
        jdbc.create(genre);
        Genre genreFromDB = jdbc.getById(7);
        assertThat(genreFromDB).hasFieldOrPropertyWithValue("genre", "Novel");
    }

}
