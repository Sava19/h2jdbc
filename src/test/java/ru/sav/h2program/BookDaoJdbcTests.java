package ru.sav.h2program;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;
import ru.sav.h2program.dao.BookDaoJdbc;
import ru.sav.h2program.model.Author;
import ru.sav.h2program.model.Book;
import ru.sav.h2program.model.Genre;

import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Тест методов BookDaoJdbc")
@JdbcTest
@Import(BookDaoJdbc.class)
public class BookDaoJdbcTests {

    @Autowired
    private BookDaoJdbc bookJdbc;

    @DisplayName("получить книгу по id")
    @Test
    void shouldGetBookFromDBById() {
        Book book = bookJdbc.getById(3);
        assertThat(book).hasFieldOrPropertyWithValue("name", "The Dog");
    }

    @DisplayName("добавить книгу")
    @Test
    void shouldCreateBook() {
        Book book = bookJdbc.getById(2);
        book.setName("Shitcode");
        bookJdbc.create(book);
        List<Book> books = bookJdbc.getAll();
        assertThat(bookJdbc.getById(7)).hasFieldOrPropertyWithValue("name", "Shitcode");
    }

    @DisplayName("обновить книгу")
    @Test
    void shouldUpdateBook() {
        Book book = bookJdbc.getById(2);
        book.setName("Ужасы говнокода!");
        bookJdbc.update(book);
        book = bookJdbc.getById(2);
        assertThat(book).hasFieldOrPropertyWithValue("name", "Ужасы говнокода!");
    }

    @DisplayName("удалить книгу")
    @Test
    void shouldDeleteBook() {
        bookJdbc.deleteById(1);
        assertThat(bookJdbc.getById(1)).isNull();
    }

    @DisplayName("получить книги")
    @Test
    void shouldGetAllBook() {
        List<Book> books = bookJdbc.getAll();
        assertThat(books).hasSizeGreaterThanOrEqualTo(2);
    }

}
