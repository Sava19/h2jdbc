package ru.sav.h2program.shell;

import org.h2.tools.Console;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ru.sav.h2program.dao.AuthorDaoJdbc;
import ru.sav.h2program.dao.BookDaoJdbc;
import ru.sav.h2program.dao.GenreDaoJdbc;
import ru.sav.h2program.model.Author;
import ru.sav.h2program.model.Book;
import ru.sav.h2program.model.Genre;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@ShellComponent
public class ShellCommands {

    private final AuthorDaoJdbc jdbcAuthor;
    private final GenreDaoJdbc jdbcGenreDao;
    private final BookDaoJdbc jdbcBookDao;

    public ShellCommands(AuthorDaoJdbc jdbcAuthor, GenreDaoJdbc jdbcGenreDao, BookDaoJdbc jdbcBookDao) {
        this.jdbcAuthor = jdbcAuthor;
        this.jdbcGenreDao = jdbcGenreDao;
        this.jdbcBookDao = jdbcBookDao;
    }

    @ShellMethod(value = "run H2 console", key = {"console"})
    public String runConsoleH2() throws SQLException {
        Console.main();
        return "Консоль H2 запущена";
    }

    @ShellMethod(value = "add Author to DB", key = {"add-author"})
    public String addAuthor(@ShellOption String fio,
                            @ShellOption String email) throws SQLException, ParseException {
        Author author = new Author(fio, email);
        jdbcAuthor.create(author);
        return String.format("Автор %s добавлен", author);
    }


    @ShellMethod(value = "add Genre to DB", key = {"add-genre"})
    public String addGenre(@ShellOption String name) throws SQLException {
        Genre genre = new Genre(name);
        jdbcGenreDao.create(genre);
        return String.format("Жанр %s добавлен", name);
    }

    @ShellMethod(value = "delete book from DB", key = {"delete-book"})
    public String deleteBook(@ShellOption long id) throws SQLException {
        jdbcBookDao.deleteById(id);
        return String.format("Книга id=%s удалена", id);
    }

    @ShellMethod(value = "get book from DB", key = {"get-book"})
    public String getBook(@ShellOption long id) throws SQLException {
        Book book = jdbcBookDao.getById(id);
        return book.toString();
    }


    @ShellMethod(value = "add book to DB", key = {"add-book"})
    public String addBook(@ShellOption String name,
                          @ShellOption String genreId,
                          @ShellOption String authorsIDs, // вводить через запятую, например 1,2
                          @ShellOption String pages) throws SQLException {

        Genre genre = jdbcGenreDao.getById(Long.parseLong(genreId));
        List<Author> authors = new ArrayList<>();
        for (String id : authorsIDs.split(",")) {
            authors.add(jdbcAuthor.getById(Long.parseLong(id)));
        }
        Book book = new Book(authors, genre, name, Integer.parseInt(pages));
        jdbcBookDao.create(book);
        return "Книга " + book.toString() + "добавлена!";
    }

    @ShellMethod(value = "update book in DB", key = {"update-book"})
    public String updateBookById(@ShellOption String id,
                                 @ShellOption String name,
                                 @ShellOption String pages,
                                 @ShellOption String genreId) throws SQLException {

        Book bookForUpdate = jdbcBookDao.getById(Long.parseLong(id));
        bookForUpdate.setId(Long.parseLong(id));
        bookForUpdate.setName(name);
        bookForUpdate.setPages(Integer.parseInt(pages));
        bookForUpdate.setGenre(jdbcGenreDao.getById(Long.parseLong(genreId)));
        jdbcBookDao.update(bookForUpdate);
        return "Книга " + bookForUpdate.toString() + "обновлена!";
    }
}