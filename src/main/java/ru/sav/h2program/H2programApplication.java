package ru.sav.h2program;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class H2programApplication {

    public static void main(String[] args) {
        SpringApplication.run(H2programApplication.class, args);
    }

}