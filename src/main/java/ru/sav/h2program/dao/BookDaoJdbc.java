package ru.sav.h2program.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Repository;
import ru.sav.h2program.model.Author;
import ru.sav.h2program.model.Book;
import ru.sav.h2program.model.Genre;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class BookDaoJdbc implements BookDao {

    private final NamedParameterJdbcOperations jdbc;

    public BookDaoJdbc(NamedParameterJdbcOperations jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Book getById(long id) {
        final Map<String,Object> params = new HashMap<>(1);
        params.put("id", id);
        String SQLQuery = "SELECT b.*, g.genre, a.fio, a.email, ab.authorID " +
                " FROM tbBook b " +
                " INNER JOIN tbGenre g ON b.genreID = g.ID " +
                " INNER JOIN tbAuthorsAndBooks ab ON b.ID = ab.bookID " +
                " INNER JOIN tbAuthor a ON ab.authorID = a.ID " +
                " WHERE b.ID = :id";
        return jdbc.query(SQLQuery,
                params,
                new BookExtractor());
    }

    @Override
    public void create(Book book) {
        String SQLQuery = "INSERT INTO tbBook (genreID, name, pages) VALUES " +
                "(:genreID, :name, :pages)";
        final Map<String,Object> params = new HashMap<>(3);
        params.put("genreID", book.getGenre().getId());
        params.put("name", book.getName());
        params.put("pages", book.getPages());
        jdbc.update(SQLQuery, params);
        SQLQuery = "SELECT MAX(ID) as maxID FROM tbBook";
        Long maxBookID = jdbc.query(SQLQuery, new MaxIDMapper()).get(0);
        System.out.println(maxBookID);
        SQLQuery = "INSERT INTO tbAuthorsAndBooks (authorID, bookID) VALUES " +
                "(:authorID, :bookID)";

        for (Author author : book.getAuthors()) {
            params.clear();
            params.put("authorID", author.getId());
            params.put("bookID", maxBookID);
            jdbc.update(SQLQuery, params);
        }


    }

    @Override
    public void update(Book book) {
        String SQLQuery = "UPDATE tbBook SET " +
                "genreID = :genreID, name = :name, pages = :pages " +
                "WHERE id = :id";
        final Map<String,Object> params = new HashMap<>(4);
        params.put("genreID", book.getGenre().getId());
        params.put("name", book.getName());
        params.put("pages", book.getPages());
        params.put("id", book.getId());
        jdbc.update(SQLQuery, params);
    }

    @Override
    public void deleteById(long book) {
        final Map<String,Object> params = new HashMap<>(1);
        params.put("bookId", book);
        String SQLQuery = "DELETE FROM tbAuthorsAndBooks WHERE bookId = :bookId";
        jdbc.update(SQLQuery, params);
        SQLQuery = "DELETE FROM tbBook WHERE id = :bookId";
        jdbc.update(SQLQuery, params);
    }

    @Override
    public List<Book> getAll() {
        String SQLQuery = "SELECT b.*, g.genre, a.fio, a.email, ab.authorID " +
                "FROM tbBook b " +
                "INNER JOIN tbGenre g ON b.genreID = g.ID " +
                "INNER JOIN tbAuthorsAndBooks ab ON b.ID = ab.bookID " +
                "INNER JOIN tbAuthor a ON ab.authorID = a.ID ";
        return jdbc.query(SQLQuery,
                new BooksExtractor());
    }


    private class BookExtractor implements ResultSetExtractor<Book> {
        @Override
        public Book extractData(ResultSet resultSet) throws SQLException, DataAccessException {
            List<Book> books = new BooksExtractor().extractData(resultSet);
            if (books.isEmpty()) return null;
            else return books.iterator().next();
        }
    }

    private class BooksExtractor implements ResultSetExtractor<List<Book>> {
        @Override
        public List<Book> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
            Map<Long, Book> books = new HashMap<>();
            while (resultSet.next()) {
                Book book = books.get(resultSet.getLong("id"));
                if (book == null) {
                    book = new Book(
                            resultSet.getLong("id"),
                            new ArrayList<Author>(),
                            new Genre(resultSet.getLong("genreID"), resultSet.getString("genre")),
                            resultSet.getString("name"),
                            resultSet.getInt("pages")
                    );
                    books.put(resultSet.getLong("id"), book);
                }
                book.getAuthors().add(new Author(
                        resultSet.getLong("authorID"),
                        resultSet.getString("fio"),
                        resultSet.getString("email")
                ));
            }
            return new ArrayList<>(books.values());
        }
    }

    private static class MaxIDMapper implements RowMapper<Long> {
        @Override
        public Long mapRow(ResultSet resultSet, int i) throws SQLException {
            return resultSet.getLong("maxID");
        }
    }


}
