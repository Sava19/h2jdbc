package ru.sav.h2program.dao;

import ru.sav.h2program.model.Book;

import java.util.List;

public interface BookDao {
    Book getById(long id);
    void create(Book book);
    void update(Book book);
    void deleteById(long book);
    List<Book> getAll();
}
