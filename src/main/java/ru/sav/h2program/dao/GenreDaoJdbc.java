package ru.sav.h2program.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Repository;
import ru.sav.h2program.model.Genre;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Repository
public class GenreDaoJdbc implements GenreDao {

    private final NamedParameterJdbcOperations jdbc;

    public GenreDaoJdbc(NamedParameterJdbcOperations jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Genre getById(long id) {
        final Map<String,Object> params = new HashMap<>(1);
        params.put("id", id);
        return jdbc.queryForObject("SELECT * FROM tbGenre WHERE ID = :id",
                params,
                new GenreMapper());
    }

    @Override
    public void create(Genre genre) {
        String SQLQuery = "INSERT INTO tbGenre (genre) VALUES " +
                "(:genre)";
        final Map<String,Object> params = new HashMap<>(1);
        params.put("genre", genre.getGenre());
        jdbc.update(SQLQuery, params);
    }

    private static class GenreMapper implements RowMapper<Genre> {
        @Override
        public Genre mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Genre(resultSet.getLong("id"),
                    resultSet.getString("genre"));
        }
    }
}
