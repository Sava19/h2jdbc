package ru.sav.h2program.dao;

import ru.sav.h2program.model.Author;

public interface AuthorDao {
    Author getById(long id);
    Author getByFio(String name);
    void create(Author author);
}
