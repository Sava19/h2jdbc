package ru.sav.h2program.dao;

import ru.sav.h2program.model.Genre;

public interface GenreDao {
    Genre getById(long id);
    void create(Genre genre);
}
