package ru.sav.h2program.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Repository;
import ru.sav.h2program.model.Author;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Repository
public class AuthorDaoJdbc implements AuthorDao {

    private final NamedParameterJdbcOperations jdbc;

    public AuthorDaoJdbc(NamedParameterJdbcOperations jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Author getById(long id) {
        final Map<String,Object> params = new HashMap<>(1);
        params.put("id", id);
        return jdbc.queryForObject("SELECT * FROM tbAuthor WHERE ID = :id",
                params,
                new AuthorMapper());
    }

    @Override
    public Author getByFio(String name) {
        final Map<String,Object> params = new HashMap<>(1);
        params.put("name", name);
        return jdbc.queryForObject("SELECT * FROM tbAuthor WHERE FIO = :name",
                params,
                new AuthorMapper());
    }

    @Override
    public void create(Author author) {
        String SQLQuery = "INSERT INTO tbAuthor (FIO, email) VALUES " +
                "(:fio, :email)";
        final Map<String,Object> params = new HashMap<>(2);
        params.put("fio", author.getFio());
        params.put("email", author.getEmail());
        jdbc.update(SQLQuery, params);
    }


    private static class AuthorMapper implements RowMapper<Author> {
        @Override
        public Author mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Author(resultSet.getLong("id"),
                    resultSet.getString("fio"),
                    resultSet.getString("email"));
        }
    }
}



