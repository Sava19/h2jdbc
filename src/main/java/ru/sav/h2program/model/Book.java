package ru.sav.h2program.model;

import java.util.List;

public class Book {

        private long id;
        private List<Author> authors;
        private Genre genre;
        private String name;
        private int pages;

        public Book() {
        }

        public Book(List<Author> authors, Genre genre, String name, int pages) {
                this.authors = authors;
                this.genre = genre;
                this.name = name;
                this.pages = pages;
        }

        public Book(long id, List<Author> authors, Genre genre, String name, int pages) {
                this.id = id;
                this.authors = authors;
                this.genre = genre;
                this.name = name;
                this.pages = pages;
        }

        public long getId() {
                return id;
        }

        public void setId(long id) {
                this.id = id;
        }

        public List<Author> getAuthors() {
                return authors;
        }

        public void setAuthors(List<Author> authors) {
                this.authors = authors;
        }

        public Genre getGenre() {
                return genre;
        }

        public void setGenre(Genre genre) {
                this.genre = genre;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public int getPages() {
                return pages;
        }

        public void setPages(int pages) {
                this.pages = pages;
        }

        @Override
        public String toString() {
                return "Book{" +
                        "id=" + id +
                        ", authors=" + authors +
                        ", genre=" + genre +
                        ", name='" + name + '\'' +
                        ", pages=" + pages +
                        '}';
        }
}
