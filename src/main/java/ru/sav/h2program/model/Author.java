package ru.sav.h2program.model;

public class Author {

    private long id;
    private String fio;
    private String email;

    public Author(String fio, String email) {
        this.fio = fio;
        this.email = email;
    }

    public Author(long id, String fio, String email) {
        this.id = id;
        this.fio = fio;
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", fio='" + fio + '\'' +
                ", email='" + email + '\'' +
                '}';
    }


}
