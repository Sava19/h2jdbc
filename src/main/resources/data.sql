INSERT INTO tbAuthor (ID, FIO, EMAIL)
VALUES
(1, 'Petrov Valeriy Ivanovich', null),
(2, 'Ivanov Ivan Dmitrievich', 'email@mail.ru'),
(3, 'Sidorov Dmitry Petrovich', 'nope@gmail.com'),
(4, 'Oleg Oleg Oleg', null),
(5, 'Oakman Eugene Eugene', 'oakman@greefield.com');

INSERT INTO tbGenre (ID, genre)
VALUES
(1, 'Story'),
(2, 'Play'),
(3, 'Drama'),
(4, 'Comedy'),
(5, 'Biography'),
(6, 'AutoBiography');


INSERT INTO tbBook (ID, genreID, name, pages)
VALUES
(1, 6, 'Life of the single oak', 5000),
(2, 5, 'Life after', 300),
(3, 4, 'The Dog', 23),
(4, 3, 'One', 341),
(5, 2, 'The Curtain', 244),
(6, 1, 'The Horse', 455);

INSERT INTO tbAuthorsAndBooks (authorID, bookID)
VALUES
(5, 1),
(1, 2),
(2, 2),
(3, 3),
(4, 4),
(4, 5),
(1, 6);